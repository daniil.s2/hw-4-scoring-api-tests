Scoring API Tests
===============

**Overview**
***
Home work for course Otus “Python Developer”<br>
Lesson: 04_testing<br>
Task: Scoring API Tests

This is declarative system for http-request description. It’s allow describe http-request as class, initialize its and manipulate request like class object. (It's similar to ORM Django or any other ORM of python web framework.)
Also there is sample of using the example simple HTTP server and storage system based on memcached (https://memcached.org/).


**Requirements:**
***
Linux Mint 19<br>
Python 3.6.7<br>
virtualenv 15.1.0<br>
memcached 1.5.6<br>
