import functools


def cases(cases):
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args):
            for c in cases:
                new_args = args + (c,)

                try:
                    f(*new_args)
                except Exception as err:
                    raise Exception('Used args as case for {}: ({},).\n{}'.format(
                        f.__name__, c, err)
                    )

        return wrapper
    return decorator
