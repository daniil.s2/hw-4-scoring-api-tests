
import unittest
from datetime import timedelta, datetime

from api import Field, FieldValidationError, CharField, ArgumentsField, \
    EmailField, PhoneField, DateField, BirthDayField, GenderField, \
    ClientIDsField
from tests.functions import cases


class TestField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = Field(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = Field(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        str(),
        list(),
        tuple(),
        dict()
    ])
    def test_validate_nullable_FAIL(self, field_val):
        field = Field(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        str(),
        list(),
        tuple(),
        dict(),
        1,
        'str_text'
    ])
    def test_validate_OK(self, field_val):
        field = Field(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)


class TestCharField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = CharField(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = CharField(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_nullable_FAIL(self):
        field = CharField(required=True, nullable=False)
        field_val = str()
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        [1,],
        (1,),
        {'k': 'v'},
        1,
        True
    ])
    def test_validate_by_type_FAIL(self, field_val):
        field = CharField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        str(),
        'str'
    ])
    def test_validate_OK(self, field_val):
        field = CharField(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)


class TestArgumentsField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = ArgumentsField(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = ArgumentsField(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_nullable_FAIL(self):
        field = ArgumentsField(required=True, nullable=False)
        field_val = dict()
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        [1,],
        (1,),
        1,
        'str_text',
        True
    ])
    def test_validate_by_type_FAIL(self, field_val):
        field = ArgumentsField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        dict(),
        {'k': 'v'}
    ])
    def test_validate_OK(self, field_val):
        field = ArgumentsField(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)


class TestEmailField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = EmailField(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = EmailField(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_nullable_FAIL(self):
        field = EmailField(required=True, nullable=False)
        field_val = str()
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        [1,],
        (1,),
        {'k': 'v'},
        1,
        True
    ])
    def test_validate_by_type_FAIL(self, field_val):
        field = EmailField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_email_FAIL(self):
        field = EmailField(required=True, nullable=False)
        field_val = 'emaildomain.com'
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        str(),
        'email@domain.com'
    ])
    def test_validate_OK(self, field_val):
        field = EmailField(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)


class TestPhoneField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = PhoneField(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = PhoneField(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_nullable_FAIL(self):
        field = PhoneField(required=True, nullable=False)
        field_val = str()
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        [1,],
        (1,),
        {'k': 'v'},
        True
    ])
    def test_validate_by_type_FAIL(self, field_val):
        field = PhoneField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        '7111111111',
        7111111111,
        '711111111111',
        711111111111,
        '81111111111',
        81111111111,
        '7A111111111'
    ])
    def test_validate_phone_FAIL(self, field_val):
        field = PhoneField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        str(),
        '71111111111',
        71111111111
    ])
    def test_validate_OK(self, field_val):
        field = PhoneField(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)


class TestDateField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = DateField(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = DateField(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_nullable_FAIL(self):
        field = DateField(required=True, nullable=False)
        field_val = str()
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        [1,],
        (1,),
        {'k': 'v'},
        True]
    )
    def test_validate_by_type_FAIL(self, field_val):
        field = DateField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        'text',
        '01.31.2019',
        '2019.01.31',
        '2019.31.01',
        '31-01-2019',
        '31/01/2019',
        '31012019',
        '31.01.19'
    ])
    def test_validate_date_FAIL(self, field_val):
        field = DateField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        str(),
        '31.01.2019'
    ])
    def test_validate_OK(self, field_val):
        field = DateField(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)


class TestBirthDayField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = BirthDayField(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = BirthDayField(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_nullable_FAIL(self):
        field = BirthDayField(required=True, nullable=False)
        field_val = str()
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        [1,],
        (1,),
        {'k': 'v'},
        True
    ])
    def test_validate_by_type_FAIL(self, field_val):
        field = BirthDayField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        'not date text',
        datetime(year=datetime.now().year, month=1, day=31).strftime('%m.%d.%Y'),  #'01.31.20XX',
        datetime(year=datetime.now().year, month=1, day=31).strftime('%Y.%m.%d'),  #'20XX.01.31',
        datetime(year=datetime.now().year, month=1, day=31).strftime('%Y.%d.%m'),  #'20XX.31.01',
        datetime(year=datetime.now().year, month=1, day=31).strftime('%d-%m-%Y'),  #'31-01-20XX',
        datetime(year=datetime.now().year, month=1, day=31).strftime('%d/%m/%Y'),  #'31/01/20XX',
        datetime(year=datetime.now().year, month=1, day=31).strftime('%d%m%Y'),  #'310120XX',
        datetime(year=datetime.now().year, month=1, day=31).strftime('%d%m%y')  #'31.01.XX'
    ])
    def test_validate_date_FAIL(self, field_val):
        field = BirthDayField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_date_more_70_years_FAIL(self):
        field = BirthDayField(required=True, nullable=False)
        more_70years_date = datetime.now() - timedelta(days=71*365)
        field_val = more_70years_date.strftime('%d.%m.%Y')
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        str(),
        datetime.now().strftime('%d.%m.%Y')
    ])
    def test_validate_OK(self, field_val):
        field = BirthDayField(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)


class TestGenderField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = GenderField(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = GenderField(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_nullable_FAIL(self):
        field = GenderField(required=True, nullable=False)
        field_val = str()
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        [1,],
        (1,),
        {'k': 'v'},
        True
    ])
    def test_validate_by_type_FAIL(self, field_val):
        field = GenderField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([3, 4, 5, 6, 7])
    def test_validate_choice_FAIL(self, field_val):
        field = GenderField(required=False, nullable=True)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        str(),
        0,
        1,
        2
    ])
    def test_validate_OK(self, field_val):
        field = GenderField(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)


class TestClientIDsField(unittest.TestCase):

    def test_validate_double_underscore_field_FAIL(self):
        field = ClientIDsField(required=False, nullable=True)
        field_name = '__dummy_field_name__'
        with self.assertRaises(FieldValidationError):
            field.validate(field_name, None)

    def test_validate_required_FAIL(self):
        field = ClientIDsField(required=True, nullable=False)
        field_val = None
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    def test_validate_nullable_FAIL(self):
        field = ClientIDsField(required=True, nullable=False)
        field_val = list()
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        (1,),
        {'k': 'v'},
        1,
        'str_text',
        True
    ])
    def test_validate_by_type_FAIL(self, field_val):
        field = ClientIDsField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        ['a', 'b', 'c'],
        [True, False, True],
        [list(), list(), list()],
        [tuple(), tuple(), tuple()],
        [dict(), dict(), dict()]
    ])
    def test_validate_by_type_id_FAIL(self, field_val):
        field = ClientIDsField(required=True, nullable=False)
        with self.assertRaises(FieldValidationError):
            field.validate('dummy_field_name', field_val)

    @cases([
        None,
        list(),
        [1, 2, 3]
    ])
    def test_validate_OK(self, field_val):
        field = ClientIDsField(required=False, nullable=True)
        result = field.validate('dummy_field_name', field_val)
        self.assertEqual(result, None)
