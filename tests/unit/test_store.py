import unittest
import time

from store import Store


class TestStoreCache(unittest.TestCase):

    def setUp(self):
        self.cache_size = 3
        self.store = Store(None, None, self.cache_size)

    def test_cache_get_OK(self):
        key, val = 'any_key', 'any_val'
        self.store.cache_set(key, val)

        result = self.store.cache_get(key)
        self.assertEqual(result, val)

    def test_cache_set_OK(self):
        key, val = 'any_key', 'any_val'
        self.store.cache_set(key, val)

        result = self.store.cache_get(key)
        self.assertEqual(result, val)

    def test_cache_set_remove_lru_item(self):
        """Check auto removing least recently used element."""
        items_len = self.cache_size + 1
        index_key, index_val = 0, 1
        items = [(i, i) for i in range(items_len)]
        first_item = items[0]

        for item in items:
            self.store.cache_set(item[index_key], item[index_val])

        for item in items:
            result = self.store.cache_get(item[index_key])
            if item != first_item:
                self.assertEqual(result, item[index_val])
            else:
                self.assertEqual(result, None)

    def test_cache_set_remove_by_ttl(self):
        """Check auto removing element by ttl."""
        key, val = 'any_key', 'any_val'
        ttl = 1

        self.store.cache_set(key, val, ttl=ttl)
        time.sleep(ttl + 1)

        result = self.store.cache_get(key)
        self.assertEqual(result, None)

        self.store.cache_set(key, val, ttl=ttl)

        result = self.store.cache_get(key)
        self.assertEqual(result, val)
