import hashlib
import unittest
from datetime import datetime

from scoring import get_score
from store import Store
from tests.functions import cases


class TestScoring(unittest.TestCase):

    def setUp(self):
        self.cache_size = 3
        self.store = Store(None, None, self.cache_size)

    def tearDown(self):
        self.store._cache._clear()

    def calculate_score(self, phone, email, birthday=None, gender=None, first_name=None, last_name=None):
        score = 0

        if phone:
            score += 1.5
        if email:
            score += 1.5
        if birthday and gender is not None:
            score += 1.5
        if first_name and last_name:
            score += 0.5

        return score

    def prepare_key(store, phone, email, birthday=None, gender=None, first_name=None, last_name=None):
        key_parts = [
            str(phone) if phone is not None else "",
            email or "",
            birthday or "",
            str(gender) if gender is not None else "",
            first_name or "",
            last_name or ""
        ]
        payload = "".join(key_parts).encode('UTF-8')
        key = "uid:" + hashlib.md5(payload).hexdigest()
        return key

    @cases([
        {'phone': '71111111111',
         'email': 'test@test.ru',
         'birthday': '11.11.1991',
         'gender': 0,
         'first_name': 'Vasy',
         'last_name': 'Pupkin'
         },
        {'phone': 71111111111,
         'email': 'test@test.ru',
         'birthday': '11.11.1991',
         'gender': 0,
         'first_name': 'Vasy'
         },
        {'phone': '71111111111',
         'email': 'test@test.ru',
         'birthday': '11.11.1991',
         'gender': 0
         },
        {'phone': '71111111111',
         'email': 'test@test.ru',
         'birthday': '11.11.1991'
         },
        {'phone': '71111111111',
         'email': 'test@test.ru'
         },
        {'phone': '71111111111',
         'email': 'test@test.ru',
         'birthday': '11.11.1991'
         },
        {'phone': '71111111111',
         'email': 'test@test.ru'
         }
    ])
    def test_get_score_write_cache_OK(self, item):
        key = self.prepare_key(**item)
        score = get_score(self.store, **item)
        cached_score = self.store.cache_get(key)
        self.assertEqual(score, cached_score)

    @cases([
        {'phone': '71111111111',
         'email': 'test@test.ru',
         'birthday': '11.11.1991',
         'gender': 0,
         'first_name': 'Vasy',
         'last_name': 'Pupkin'
         },
        {'phone': 71111111111,
         'email': 'test@test.ru',
         'birthday': '11.11.1991',
         'gender': 0,
         'first_name': 'Vasy'
         },
        {'phone': '71111111111',
         'email': 'test@test.ru',
         'birthday': '11.11.1991',
         'gender': 0
         },
        {'phone': '71111111111',
         'email': 'test@test.ru',
         'birthday': '11.11.1991'
         },
        {'phone': '71111111111',
         'email': 'test@test.ru'
         },
        {'phone': '71111111111',
         'email': 'test@test.ru',
         'birthday': '11.11.1991'
         },
        {'phone': '71111111111',
         'email': 'test@test.ru'
         }
    ])
    def test_get_score_read_cache_OK(self, item):
        key = self.prepare_key(**item)
        score = self.calculate_score(**item)
        self.store.cache_set(key, score)
        cached_score = get_score(self.store, **item)
        self.assertEqual(score, cached_score)
