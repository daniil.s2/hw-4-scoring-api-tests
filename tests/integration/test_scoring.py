import unittest

from scoring import get_interests
from store import Store


class TestScoring(unittest.TestCase):

    def setUp(self):
        self.host = 'localhost'
        self.port = 11211
        self.store = Store(self.host, self.port, None, autoconnect=True)

    def tearDown(self):
        self.store.close()

    def test_get_interests_OK(self):
        interests = ["cars", "pets", "travel", "hi-tech", "sport", "music", "books", "tv", "cinema", "geek", "otus"]
        cid = '1'
        self.store.set("i:%s" % cid, interests)

        result = get_interests(self.store, cid)
        self.assertEqual(result, interests)
