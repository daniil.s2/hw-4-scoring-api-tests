import unittest

from pymemcache import MemcacheIllegalInputError

from store import Store
from tests.functions import cases


class TestStore(unittest.TestCase):

    def setUp(self):
        self.host = 'localhost'
        self.port = 11211
        self.store = Store(self.host, self.port, None, autoconnect=True)
        self.store._storage.flush_all()

    def tearDown(self):
        self.store.close()
        self.store._storage.flush_all()

    @cases([
        {'key': str(i), 'val': str(i)} for i in range(5)
    ])
    def test_get_OK(self, item):
        self.store.set(item['key'], item['val'])
        val = self.store._json_serializer(None, item['val'])[0]
        result = self.store.get(item['key'])

        self.assertEqual(val, result)

    @cases([
        str(i) for i in range(5)
    ])
    def test_get_nonexistent_val_OK(self, key):
        self.store._storage.delete(key)

        result = self.store.get(key)

        self.assertEqual(None, result)

    @cases([
        {'key': 'none', 'val': None},
        {'key': '1', 'val': 1},
        {'key': '1.1', 'val': 1.1},
        {'key': 'bool', 'val': True},
        {'key': 'list', 'val': [1, 'A', False]},
        {'key': 'tuple', 'val': (1, 'A', True)},
        {'key': 'dict', 'val': {1: 1, 2: 'A', 3: True}}
    ])
    def test_set_get_json_serialization_OK(self, item):
        self.store.set(item['key'], item['val'])
        serialized_val = self.store._json_serializer(None, item['val'])[0]
        result = self.store.get(item['key'])

        self.assertEqual(serialized_val, result)

    @cases([
        'ЫЫЫ',
        'èèè',
        'ζζζ'
    ])
    def test_set_non_ascii_val_FAIL(self, val):
        with self.assertRaises(MemcacheIllegalInputError):
            self.store.set('any_key', val)

    @cases([
        'ЫЫЫ',
        'èèè',
        'ζζζ'
    ])
    def test_set_non_ascii_key_FAIL(self, key):
        with self.assertRaises(MemcacheIllegalInputError):
            self.store.set(key, 'any_val')

    @cases([
        1,
        1.1,
        True,
        None
    ])
    def test_set_non_str_key_FAIL(self, key):
        with self.assertRaises(TypeError):
            self.store.set(key, 'any_val')
