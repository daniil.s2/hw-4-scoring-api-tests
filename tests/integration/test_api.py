import datetime
import hashlib
import unittest

import api
from store import Store
from tests.functions import cases


class TestSuite(unittest.TestCase):
    def setUp(self):
        self.context = {}
        self.headers = {}
        self.store = Store('localhost', 11211, 256, autoconnect=True)

    def tearDown(self):
        self.store._cache._clear()
        self.store.close()

    def get_response(self, request):
        return api.method_handler({"body": request, "headers": self.headers}, self.context, self.store)

    def set_valid_auth(self, request):
        if request.get("login") == api.ADMIN_LOGIN:
            data = datetime.datetime.now().strftime("%Y%m%d%H") + api.ADMIN_SALT
            request["token"] = hashlib.sha512(data.encode('UTF-8')).hexdigest()
        else:
            msg = request.get("account", "") + request.get("login", "") + api.SALT
            request["token"] = hashlib.sha512(msg.encode('UTF-8')).hexdigest()

    def calculate_score(self, phone, email, birthday=None, gender=None, first_name=None, last_name=None):
        score = 0

        if phone:
            score += 1.5
        if email:
            score += 1.5
        if birthday and gender is not None:
            score += 1.5
        if first_name and last_name:
            score += 0.5

        return score

    @cases([
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "", "arguments": {}},
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "sdd", "arguments": {}},
        {"account": "horns&hoofs", "login": "admin", "method": "online_score", "token": "", "arguments": {}},
    ])
    def test_bad_auth(self, request):
        _, code = self.get_response(request)
        self.assertEqual(api.FORBIDDEN, code)

    @cases([
        {},
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score"},
        {"account": "horns&hoofs", "login": "h&f", "method": "AZAZAZA", "arguments": {}},
        {"account": "horns&hoofs", "method": "online_score", "arguments": {}},
    ])
    def test_invalid_method_request(self, request):
        self.set_valid_auth(request)
        _, code = self.get_response(request)
        self.assertEqual(api.INVALID_REQUEST, code)

    @cases([
        {},
        {"phone": "79175002040"},
        {"phone": "89175002040", "email": "stupnikov@otus.ru"},
        {"phone": 89175002040, "email": "stupnikov@otus.ru"},
        {"phone": "79175002040", "email": "stupnikovotus.ru"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": -1},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": "1"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.1890"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "XXX"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000", "first_name": 1},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000", "first_name": "s", "last_name": 2},
        {"phone": "79175002040", "birthday": "01.01.2000", "first_name": "s"},
        {"email": "stupnikov@otus.ru", "gender": 1, "last_name": 2},
    ])
    def test_invalid_online_score(self, arguments):
        method = "online_score"
        request = {"account": "horns&hoofs", "login": "h&f", "method": method, "arguments": arguments}
        self.set_valid_auth(request)
        _, code = self.get_response(request)
        self.assertEqual(api.INVALID_REQUEST, code)

    @cases([
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991'},
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991', 'gender': 0, 'first_name': 'Vasy', 'last_name': 'Pupkin'},
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991', 'gender': 1, 'first_name': 'Vasy'},
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991', 'gender': 2},
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991'},
        {'phone': 71111111111, 'email': 'test@test.ru'},
        {'phone': 71111111111, 'email': 'test@test.ru', 'birthday': '11.11.1991'},
    ])
    def test_online_score_OK(self, arguments):
        method = "online_score"
        request = {"account": "horns&hoofs", "login": "h&f", "method": method, "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        score = self.calculate_score(**arguments)
        self.assertEqual(score, response.get('score'))
        self.assertEqual(api.OK, code)

    @cases([
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991'},
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991', 'gender': 0, 'first_name': 'Vasy', 'last_name': 'Pupkin'},
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991', 'gender': 1, 'first_name': 'Vasy'},
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991', 'gender': 2},
        {'phone': '71111111111', 'email': 'test@test.ru', 'birthday': '11.11.1991'},
        {'phone': 71111111111, 'email': 'test@test.ru'},
        {'phone': 71111111111, 'email': 'test@test.ru', 'birthday': '11.11.1991'},
    ])
    def test_online_score_admin_OK(self, arguments):
        method = "online_score"
        request = {"account": "horns&hoofs", "login": "admin", "method": method, "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        score = 42
        self.assertEqual(score, response.get('score'))
        self.assertEqual(api.OK, code)

    @cases([
        {},
        {"date": "20.07.2017"},
        {"client_ids": [], "date": "20.07.2017"},
        {"client_ids": {1: 2}, "date": "20.07.2017"},
        {"client_ids": ["1", "2"], "date": "20.07.2017"},
        {"client_ids": [1, 2], "date": "XXX"},
    ])
    def test_invalid_clients_interests(self, arguments):
        method = "clients_interests"
        request = {"account": "horns&hoofs", "login": "h&f", "method": method, "arguments": arguments}
        self.set_valid_auth(request)
        _, code = self.get_response(request)
        self.assertEqual(api.INVALID_REQUEST, code)

    @cases([
        {"client_ids": [1, 2, 3], "date": datetime.datetime.today().strftime("%d.%m.%Y")},
        {"client_ids": [1, 2], "date": "19.07.2017"},
        {"client_ids": [0]},
    ])
    def test_clients_interests_OK(self, arguments):
        interests = ["cars", "pets", "travel", "hi-tech", "sport", "music"]
        for cid in arguments.get("client_ids"):
            self.store.set("i:%s" % cid, interests)

        method = "clients_interests"
        request = {"account": "horns&hoofs", "login": "h&f", "method": method, "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)

        for c_interests in response.values():
            self.assertEqual(interests, c_interests)

        self.assertEqual(api.OK, code)


if __name__ == "__main__":
    unittest.main()
