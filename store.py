import json

from cacheout import LRUCache
from pymemcache.exceptions import MemcacheUnexpectedCloseError
from pymemcache.client import base


class Store:
    _storage = None

    def __init__(self,
                 host,
                 port,
                 cache_size,
                 connection_timeout=None,
                 timeout=None,
                 connection_retries=1,
                 autoconnect=False
                 ):

        self.host = host
        self.port = port
        self.connection_timeout = connection_timeout
        self.timeout = timeout
        self.connection_retries = connection_retries

        if autoconnect:
            self.connect()

        self._cache = LRUCache(maxsize=cache_size)

    def connect(self):
        last_attempt = self.connection_retries - 1
        for i in range(self.connection_retries):
            try:
                storage = base.Client(
                    (self.host, self.port),
                    serializer=self._json_serializer,
                    connect_timeout=self.connection_timeout,
                    timeout=self.timeout
                )
            except (MemcacheUnexpectedCloseError, ConnectionRefusedError):
                if i == last_attempt:
                    # raise exception if it's last attempt
                    raise
                else:
                    pass
            else:
                self._storage = storage
                break

    def _json_serializer(self, key, value):
        if isinstance(value, str):
            return value, 1
        return json.dumps(value), 2

    def close(self):
        self._storage.close()

    def get(self, key):
        result = self._storage.get(key)
        result = result.decode('ascii') if result else None
        return result

    def set(self, key, value):
        """Key - only str and ascii-characters.
        Value - only json serializable and ascii-characters.
        """
        return self._storage.set(key, value)

    def cache_get(self, key):
        return self._cache.get(key)

    def cache_set(self, key, value, ttl=None):
        self._cache.set(key, value, ttl=ttl)
